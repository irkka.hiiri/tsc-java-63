<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project List</h1>
<table width="100%" cellpadding="10" border="1" style="margin-top: 20px">
    <tr>
        <th width="200" nowrap="nowrap">id</th>
        <th width="200" nowrap="nowrap" align="left">name</th>
        <th width="500">description</th>
        <th width="100">created</th>
        <th width="100">started</th>
        <th width="100">finished</th>
        <th width="100">status</th>
        <th width="100" nowrap="nowrap" align="center">Edit</th>
        <th width="100" nowrap="nowrap" align="center">Remove</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}" />
            </td>
            <td>
                <c:out value="${project.name}" />
            </td>
            <td>
                <c:out value="${project.description}" />
            </td>
            <td>
                <c:out value="${project.created}" />
            </td>
            <td>
                <c:out value="${project.dateStart}" />
            </td>
            <td>
                <c:out value="${project.dateFinish}" />
            </td>
            <td>
                <c:out value="${project.status}" />
            </td>
            <td align="center">
                <a href="/project/edit/?id=${project.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/project/remove/?id=${project.id}">Remove</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px">
    <button>Create</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
