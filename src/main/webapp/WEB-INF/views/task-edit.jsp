<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit task</h1>

<form action="/task/edit?id=${task.id}" method="post">
    <input type="hidden" name="id" value="${task.id}" />
    <p>
        <div>Name:</div>
    <div><input type="text" name="name" value="${task.name}" /></div>
    </p>
    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${task.description}" /></div>
    </p>

    <button type="submit">Save</button>
</form>
<jsp:include page="../include/_footer.jsp" />